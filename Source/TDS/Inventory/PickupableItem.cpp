// Fill out your copyright notice in the Description page of Project Settings.


#include "PickupableItem.h"

#include "Components/BillboardComponent.h"
#include "Components/SphereComponent.h"
#include "Engine/CollisionProfile.h"
#include "GameFramework/RotatingMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TDS/Character/TDSCharacter.h"

// Sets default values
APickupableItem::APickupableItem()
{
    PrimaryActorTick.bCanEverTick = true;

    CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
    CollisionSphere->SetSphereRadius(100.f);
    CollisionSphere->SetCanEverAffectNavigation(false);
    CollisionSphere->SetNotifyRigidBodyCollision(true);
    CollisionSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

    RootComponent = CollisionSphere;

    BillboardComponent = CreateDefaultSubobject<UBillboardComponent>(TEXT("Billboard"));
    BillboardComponent->SetupAttachment(RootComponent);

    ParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particle System"));
    ParticleSystemComponent->SetupAttachment(RootComponent);

    ItemStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
    ItemStaticMesh->SetupAttachment(RootComponent);

    RotatingMovementComponent = CreateDefaultSubobject<URotatingMovementComponent>(TEXT("Rotating Movement Component"));
    RotatingMovementComponent->Velocity = FVector::LeftVector * 20;
}

// Called when the game starts or when spawned
void APickupableItem::BeginPlay()
{
    Super::BeginPlay();
    CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &APickupableItem::PickUp);
    CollisionSphere->OnComponentEndOverlap.AddDynamic(this, &APickupableItem::PickUpEndOverlap);
    GameInstance = Cast<UTDSGameInstance>(GetGameInstance());

    FWeaponParams WeaponParams;
    FAmmoParams AmmoParams;

    switch (Type)
    {
    case EItemType::Ammo:
        if (GameInstance->GetAmmoInfoByName(Name, AmmoParams))
        {
            ItemStaticMesh->SetStaticMesh(AmmoParams.PickupMesh);
            ParticleSystemComponent->SetTemplate(AmmoParams.PickupParticle);
            if (!SoundPickup)
            {
                SoundPickup = AmmoParams.PickupSound;
            }
        }
        break;
    case EItemType::Weapon:
        if (GameInstance->GetWeaponInfoByName(Name, WeaponParams))
        {
            ItemStaticMesh->SetStaticMesh(WeaponParams.PickupMesh);
            ParticleSystemComponent->SetTemplate(WeaponParams.PickupParticle);
            if (!SoundPickup)
            {
                SoundPickup = WeaponParams.PickupSound;
            }
        }
        break;
    }
}

void APickupableItem::PickUp(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
    if (OtherActor)
    {
        ATDSCharacter* Character = Cast<ATDSCharacter>(OtherActor);
        if (Character && !bIsNowDropped)
        {
            bool bPickedUp = false;

            UInventoryComponent* InventoryComponent = Character->InventoryComponent;

            switch (Type)
            {
            case EItemType::Ammo:
                bPickedUp = PickUpAmmo(InventoryComponent);
                break;
            case EItemType::Weapon:
                bPickedUp = PickUpWeapon(InventoryComponent);
                break;
            }

            if (bPickedUp)
            {
                UGameplayStatics::SpawnSoundAtLocation(GetWorld(), SoundPickup, GetActorLocation());
                OnPickUp(InventoryComponent, Type);
                Destroy();
            }
        }
    }
}

void APickupableItem::PickUpEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
    if (OtherActor)
    {
        ATDSCharacter* Character = Cast<ATDSCharacter>(OtherActor);
        if (Character)
        {
            bIsNowDropped = false;
        }
    }
}

bool APickupableItem::PickUpAmmo(UInventoryComponent* InventoryComponent)
{
    bool bAmmoPickedUp = false;

    FAmmoParams InitAmmoInfo;
    if (GameInstance->GetAmmoInfoByName(Name, InitAmmoInfo))
    {
        bAmmoPickedUp = InventoryComponent->AddAmmo(Name, AmmoCount, InitAmmoInfo.MaxCount);
    }

    return bAmmoPickedUp;
}

bool APickupableItem::PickUpWeapon(UInventoryComponent* InventoryComponent)
{
    bool bWeaponPickedUp = false;

    FWeaponParams InitWeaponInfo;
    if (GameInstance->GetWeaponInfoByName(Name, InitWeaponInfo))
    {
        bWeaponPickedUp = InventoryComponent->AddWeapon(Name, InitWeaponInfo.NumberRound);
    }

    return bWeaponPickedUp;
}

void APickupableItem::OnPickUp_Implementation(UInventoryComponent* InventoryComponent, EItemType ItemType)
{
    // in BP
}
