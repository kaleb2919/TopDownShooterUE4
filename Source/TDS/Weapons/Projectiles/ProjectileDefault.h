#pragma once

#include "CoreMinimal.h"
#include "ProjectileBase.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "ProjectileDefault.generated.h"

UCLASS()
class TDS_API AProjectileDefault : public AProjectileBase
{
	GENERATED_BODY()
	
public:
	AProjectileDefault();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USphereComponent* BulletCollisionSphere = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UProjectileMovementComponent* BulletProjectileMovement = nullptr;

	virtual void InitProjectile(const FProjectileParams& InitProjectileParams) override;
};
