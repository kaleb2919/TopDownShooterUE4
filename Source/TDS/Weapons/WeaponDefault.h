// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "TDS/FuncLibrary/Types.h"
#include "TDS/Weapons/Projectiles/ProjectileDefault.h"
#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart,UAnimMontage*,CharAnim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, FName, AmmoName, int, AmmoLoaded);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponFire,UAnimMontage*,FireAim,UAnimMontage*,FireAimAnim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponEmptyAmmo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponClipChanged, int, AmmoClip);

UCLASS()
class TDS_API AWeaponDefault : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    AWeaponDefault();

    UPROPERTY(VisibleAnywhere, BlueprintAssignable)
    FOnWeaponFire OnWeaponFire;
    UPROPERTY(VisibleAnywhere, BlueprintAssignable)
    FOnWeaponReloadStart OnWeaponReloadStart;
    UPROPERTY(VisibleAnywhere, BlueprintAssignable)
    FOnWeaponReloadEnd OnWeaponReloadEnd;
    UPROPERTY(VisibleAnywhere, BlueprintAssignable)
    FOnWeaponClipChanged OnWeaponClipChanged;
    UPROPERTY(VisibleAnywhere, BlueprintAssignable)
    FOnWeaponEmptyAmmo OnWeaponEmptyAmmo;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class USceneComponent* SceneComponent = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class UStaticMeshComponent* StaticMeshWeapon = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class UArrowComponent* ShootLocation = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class UArrowComponent* MagazineDropLocation = nullptr;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
    class UArrowComponent* ShellDropLocation = nullptr;

    UPROPERTY()
    FWeaponParams WeaponParams;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
    bool bIsFiring = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
    bool bIsReloading = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
    bool bIsBlockFire = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
    float CurrentDispersion = 0.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
    float CurrentDamage = 0.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
    int32 RoundLeft = 0;
    int AmmoToLoad;

    float FireTimer = 0.0f;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
    float ReloadTimer = 0.0f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
    bool ShowDebug = false;

    void WeaponInit(FWeaponParams InitWeaponParams);

    virtual void Tick(float DeltaTime) override;
    void FireTick(float DeltaTime);
    void ReloadTick(float DeltaTime);

    UFUNCTION(BlueprintCallable)
    void SetFireState(bool bIsFire);

    UFUNCTION(BlueprintCallable)
    void Fire();
    UFUNCTION(BlueprintCallable)
    void Reload(int AmmoCount);
    UFUNCTION(BlueprintCallable)
    void FinishReload();
    UFUNCTION(BlueprintCallable)
    void UpdateDispersionState(EMovementState NewMovementState);

    UFUNCTION(BlueprintCallable)
    float PlayAnimMontage(UAnimMontage* AnimMontage, bool IsLooping, float RateScale);

    UFUNCTION(BlueprintCallable)
    UAnimMontage* GetFireAnimation();
    UFUNCTION(BlueprintCallable)
    float GetFireRate();
    UFUNCTION(BlueprintCallable)
    float GetReloadRate();
    UFUNCTION(BlueprintCallable)
    int32 GetClipSize();
    UFUNCTION(BlueprintCallable)
    int32 GetNumberRoundLeft();
    UFUNCTION(BlueprintCallable)
    float GetCurrentDispersion();
    UFUNCTION(BlueprintCallable)
    int32 GetNumberProjectileByShot();
    UFUNCTION(BlueprintCallable)
    bool GetBlockFireState();
    UFUNCTION(BlueprintCallable)
    FName GetAmmoType();

    void DropMesh(const UArrowComponent* SpawnLocation, FSpawnStaticMeshParam SpawnStaticMeshParam);
};
