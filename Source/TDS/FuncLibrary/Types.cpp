// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"

bool FAmmoSlot::operator==(const FAmmoSlot& Other) const
{
    return (Name == Other.Name);
}

bool FWeaponSlot::operator==(const FWeaponSlot& Other) const
{
    return (Name == Other.Name);
}
